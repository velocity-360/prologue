import React, { Component } from 'react'

class Logo extends Component {
	render(){
		return (
			<div id="logo">
				<span className="image avatar48"><img src="https://placehold.it/150x150" alt="" /></span>
				<h1 id="title">Jane Doe</h1>
				<p>Hyperspace Engineer</p>
			</div>
		)
	}
}

export default Logo