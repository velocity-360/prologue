import React, { Component } from 'react'

export default (props) => {

	return (
		<article className="item">
			<a href="#" className="image fit"><img src={props.image} alt="" /></a>
			<header>
				<h3>{props.caption}</h3>
			</header>
		</article>
	)
}
