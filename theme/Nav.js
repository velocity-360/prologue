import React, { Component } from 'react'
import SocialIcons from './SocialIcons'
const avatar = require('../../assets/images/avatar.jpg')

class Nav extends Component {
	render(){
		return (
			<div id="header">
				<div className="top">

					<div id="logo">
						<span className="image avatar48">
							<img src={avatar} alt="" />
						</span>
						<h1 id="title">Jane Doe</h1>
						<p>Hyperspace Engineer</p>
					</div>

					<nav id="nav">
						<ul>
							<li><a href="#top" id="top-link" className="skel-layers-ignoreHref"><span className="icon fa-home">Intro</span></a></li>
							<li><a href="#portfolio" id="portfolio-link" className="skel-layers-ignoreHref"><span className="icon fa-th">Portfolio</span></a></li>
							<li><a href="#about" id="about-link" className="skel-layers-ignoreHref"><span className="icon fa-user">About Me</span></a></li>
							<li><a href="#contact" id="contact-link" className="skel-layers-ignoreHref"><span className="icon fa-envelope">Contact</span></a></li>
						</ul>
					</nav>

				</div>

				<div className="bottom">
					<SocialIcons />
				</div>

			</div>

		)
	}
}

export default Nav