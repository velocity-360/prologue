import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import Footer from './Footer'
import Nav from './Nav'
import Item from './Item'
import Form from './Form'

// images:
const pic2 = require('../../assets/images/pic02.jpg')
const pic3 = require('../../assets/images/pic03.jpg')
const pic4 = require('../../assets/images/pic04.jpg')
const pic5 = require('../../assets/images/pic05.jpg')
const pic6 = require('../../assets/images/pic06.jpg')
const pic7 = require('../../assets/images/pic07.jpg')
const pic8 = require('../../assets/images/pic08.jpg')


export default (props) => {

	return (
		<div>
			<Nav />
			<div id="main">
				<section id="top" className="one dark cover">
					<div className="container">
						<header>
							<h2 className="alt">Hi! Im <strong>Prologue</strong>, a <a href="http://html5up.net/license">free</a> responsive<br />
							site template designed by <a href="http://html5up.net">HTML5 UP</a>.</h2>
							<p>Ligula scelerisque justo sem accumsan diam quis<br />
							vitae natoque dictum sollicitudin elementum.</p>
						</header>

						<footer>
							<a href="#portfolio" className="button scrolly">Magna Aliquam</a>
						</footer>
					</div>
				</section>

				<section id="portfolio" className="two">
					<div className="container">
						<header><h2>Portfolio</h2></header>
						<p>Vitae natoque dictum etiam semper magnis enim feugiat convallis convallis
						egestas rhoncus ridiculus in quis risus amet curabitur tempor orci penatibus.
						Tellus erat mauris ipsum fermentum etiam vivamus eget. Nunc nibh morbi quis
						fusce hendrerit lacus ridiculus.</p>

						<div className="row">
							<div className="4u 12u$(mobile)">
								<Item image={pic2} caption="Ipsum Feugiat" />
								<Item image={pic3} caption="Rhoncus Semper" />
							</div>

							<div className="4u 12u$(mobile)">
								<Item image={pic4} caption="Magna Nullam" />
								<Item image={pic5} caption="Natoque Vitae" />
							</div>

							<div className="4u$ 12u$(mobile)">
								<Item image={pic6} caption="Dolor Penatibus" />
								<Item image={pic7} caption="Orci Convallis" />
							</div>
						</div>
					</div>
				</section>

				<section id="about" className="three">
					<div className="container">
						<header><h2>About Me</h2></header>

						<a href="#" className="image featured"><img src={pic8} alt="" /></a>
						<p>Tincidunt eu elit diam magnis pretium accumsan etiam id urna. Ridiculus
						ultricies curae quis et rhoncus velit. Lobortis elementum aliquet nec vitae
						laoreet eget cubilia quam non etiam odio tincidunt montes. Elementum sem
						parturient nulla quam placerat viverra mauris non cum elit tempus ullamcorper
						dolor. Libero rutrum ut lacinia donec curae mus vel quisque sociis nec
						ornare iaculis.</p>
					</div>
				</section>

				<section id="contact" className="four">
					<div className="container">
						<header><h2>Contact</h2></header>
						<p>Elementum sem parturient nulla quam placerat viverra
						mauris non cum elit tempus ullamcorper dolor. Libero rutrum ut lacinia
						donec curae mus. Eleifend id porttitor ac ultricies lobortis sem nunc
						orci ridiculus faucibus a consectetur. Porttitor curae mauris urna mi dolor.</p>
						<Form />
					</div>
				</section>

			</div>

			<Footer />
		</div>
	) 

}

