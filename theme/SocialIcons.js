import React, { Component } from 'react'

class SocialIcons extends Component {
	render(){
		return (
			<div className="bottom">
				<ul className="icons">
					<li><a href="#" className="icon fa-twitter"><span className="label">Twitter</span></a></li>
					<li><a href="#" className="icon fa-facebook"><span className="label">Facebook</span></a></li>
					<li><a href="#" className="icon fa-github"><span className="label">Github</span></a></li>
					<li><a href="#" className="icon fa-dribbble"><span className="label">Dribbble</span></a></li>
					<li><a href="#" className="icon fa-envelope"><span className="label">Email</span></a></li>
				</ul>
			</div>
		)
	}
}

export default SocialIcons