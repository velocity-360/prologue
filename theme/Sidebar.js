import React, { Component } from 'react'
import Nav from './Nav'
import Logo from './Logo'
import SocialIcons from './SocialIcons'

class Sidebar extends Component {
	render(){
		return (
			<div id="header">
				<div className="top">
					<Logo />
					<Nav />
				</div>

				<SocialIcons />
			</div>
		)
	}
}

export default Sidebar