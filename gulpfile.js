// including plugins
var gulp = require('gulp')
var minifyCSS = require('gulp-minify-css')
var autoprefixer = require('gulp-autoprefixer')
var gp_concat = require('gulp-concat')
var gp_rename = require('gulp-rename')
var gp_uglify = require('gulp-uglify')
var to5 = require('gulp-6to5')
var path = require('path')


gulp.task('css', function(){
    return gulp.src(
            [
                './assets/css/font-awesome.min.css',
                './assets/css/ie8.css',
                './assets/css/ie9.css',
                './assets/css/main.css'
            ]
        )
        .pipe(minifyCSS())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gp_concat('style.min.css'))
        .pipe(gulp.dest('./dist/css/'))
})

gulp.task('copy-fonts', function(){
    return gulp.src(
            ['./assets/fonts/**']
        )
        .pipe(gulp.dest('./dist/fonts/'))
})

gulp.task('copy-images', function(){
    return gulp.src(
            ['./assets/css/images/**.png', './assets/images/**.svg', './assets/images/**.gif']
        )
        .pipe(gulp.dest('./dist/css/images/'))
})

gulp.task('style', ['css', 'copy-fonts', 'copy-images'], function(){})


gulp.task('js', function(){
    return gulp.src(
            [
                './assets/js/jquery.min.js',
                './assets/js/jquery.scrolly.min.js',
                './assets/js/jquery.scrollzer.min.js',
                './assets/js/skel.min.js',
                './assets/js/util.js',
                './assets/js/main.js'
            ]
        )
        .pipe(gp_concat('vendor.min.js'))
        .pipe(gulp.dest('./dist/js/'))
        .pipe(gp_rename('vendor.min.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('./dist/js/'))
});

gulp.task('es6-es5', ['js'], function(){
    return gulp.src([
                './src/*/**.js',
                './src/*/*/**.js'
            ]
        )
        .pipe(to5())
        .pipe(gulp.dest('./dist/es5/'))
});

gulp.task('watch', function() {
    gulp.watch(['./src/*/**.js', './src/*/*/**.js', './assets/js/**.js'], ['es6-es5'])
})

gulp.task('prod', ['style', 'es6-es5'], function(){})

gulp.task('default', ['es6-es5', 'watch'], function(){})

